import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MyLibService {

  constructor() { }

  /**
   * Prueba función de librería
   */
  name = (data:any) =>{
    return "Bienvenido " + data;
  }

  
}
