import { Component } from '@angular/core';
import { MyLibService } from 'my-lib';
import { FooterTestLibService } from 'projects/librerias/footer-test-lib/src/public-api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  test:string;
  test_aplication: number;

  title = 'footer-app';

  constructor(private MyLibServices: MyLibService, private FooterLibServices: FooterTestLibService){
    this.test = this.MyLibServices.name("a aplicaciones");
    this.test_aplication = this.FooterLibServices.getRandom();
  }

}
