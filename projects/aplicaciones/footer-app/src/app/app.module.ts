import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MyLibModule } from 'my-lib';
import { FooterTestLibModule } from 'projects/librerias/footer-test-lib/src/lib/footer-test-lib.module';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MyLibModule,
    FooterTestLibModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
