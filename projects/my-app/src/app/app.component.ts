import { Component, OnInit } from '@angular/core';
import { MyLibService } from 'my-lib';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  title = 'my-app';

  constructor(private MyLibServices: MyLibService){}

  ngOnInit(): void {
    console.log(this.MyLibServices.name("paco"));
    //const t = this.MyLibComponents.name("fracisco");
    //console.log(t);
  }

}
