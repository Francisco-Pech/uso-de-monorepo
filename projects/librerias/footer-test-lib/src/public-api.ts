/*
 * Public API Surface of footer-test-lib
 */

export * from './lib/footer-test-lib.service';
export * from './lib/footer-test-lib.component';
export * from './lib/footer-test-lib.module';
