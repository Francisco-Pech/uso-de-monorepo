import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterTestLibComponent } from './footer-test-lib.component';

describe('FooterTestLibComponent', () => {
  let component: FooterTestLibComponent;
  let fixture: ComponentFixture<FooterTestLibComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FooterTestLibComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterTestLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
