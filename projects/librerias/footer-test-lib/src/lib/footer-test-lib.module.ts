import { NgModule } from '@angular/core';
import { FooterTestLibComponent } from './footer-test-lib.component';



@NgModule({
  declarations: [FooterTestLibComponent],
  imports: [
  ],
  exports: [FooterTestLibComponent]
})
export class FooterTestLibModule { }
