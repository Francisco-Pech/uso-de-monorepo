import { TestBed } from '@angular/core/testing';

import { FooterTestLibService } from './footer-test-lib.service';

describe('FooterTestLibService', () => {
  let service: FooterTestLibService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FooterTestLibService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
